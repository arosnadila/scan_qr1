import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Home Page",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.blueGrey,
      ),
      body: Column(
          children: [
          Text("Hello Word"),
      Padding(
        padding: const EdgeInsets.all(16.0),
        child: TextFormField(
          decoration: InputDecoration(
              hintText: "Massukkan Nama",
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                    width: 1,
                    color: Colors.black45,
                  ))),
        ),
      ),
      ElevatedButton(
        onPressed: () {},
        child: Text("Simpan"),
        style: ElevatedButton.styleFrom(
            backgroundColor: Color(0xFFF80A0A)
        ),
      ),
        ],
      ),
    );
  }
}
